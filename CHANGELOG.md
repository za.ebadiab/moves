Version 1.0.0 (2018-10-18)
- using smartLocation to find current location 
- displaying current address
- using different style for day and night (18 to 6 for night)
- adding random markers around current location after each location change and remove previous markers