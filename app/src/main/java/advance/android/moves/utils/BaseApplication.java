package advance.android.moves.utils;

import android.app.Application;
import android.graphics.Typeface;

//import com.cedarstudios.cedarmapssdk.CedarMaps;
import com.google.android.gms.maps.GoogleMap;

public class BaseApplication extends Application {

    public static BaseApplication baseApp;
    public static Typeface typeface;
    public static GoogleMap mMap;

    @Override
    public void onCreate() {
        super.onCreate();

        baseApp = this;
        typeface = Typeface.createFromAsset(getAssets(), Constants.appFontName);

/*
        CedarMaps.getInstance()
                .setClientID("...")
                .setClientSecret("...")
                .setContext(this);
*/
    }

}

