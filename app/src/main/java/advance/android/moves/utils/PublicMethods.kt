package advance.android.moves.utils

import android.content.Context
import android.widget.Toast

class PublicMethods {

    companion object {
        fun toast(mContxt: Context, msg: String) {
            Toast.makeText(mContxt, msg, Toast.LENGTH_LONG).show()
        }

    }

}