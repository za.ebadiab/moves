package advance.android.moves.services;

import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.util.List;


import advance.android.moves.utils.BaseApplication;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;

public class SmartLocationFetchAddressService extends Service {

    double lat, lon;

    @Override
    public void onCreate() {
        super.onCreate();

        EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        getAddress();

        return super.onStartCommand(intent, flags, startId);
    }


    void getAddress() {
        Location location = new Location("providername");

        location.setLatitude(lat);
        location.setLongitude(lon);

        SmartLocation.with(BaseApplication.baseApp).geocoding()
                .reverse(location, new OnReverseGeocodingListener() {

                    @Override
                    public void onAddressResolved(Location location, List<Address> list) {
                        if (list.size() != 0)

                            EventBus.getDefault().post(new AddressModel(list.get(0)));

                    }
                });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    /*
        public void getAddress() {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                if (addresses.size() != 0) {
                    Address obj = addresses.get(0);
                    String address = obj.getAddressLine(0);
                    AddressModel addressModel = new AddressModel(address);

                 //   EventBus.getDefault().post(addressModel);

                 }

            } catch (IOException e) {
                e.printStackTrace();
       //         Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

      */
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onReceivedLatLon(LatLonModel latLonModel) {

        lat = latLonModel.lat;
        lon = latLonModel.lon;
    }
}
