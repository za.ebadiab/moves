package advance.android.moves.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;

import advance.android.moves.utils.BaseApplication;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;

public class SmartLocationService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        getMyLocation(BaseApplication.mMap);

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    void getMyLocation(GoogleMap mMap) {

        SmartLocation.with(this)
                .location()
                //  .oneFix()
                .continuous()
                .config(LocationParams.BEST_EFFORT)
                .start(L -> {
                    BaseApplication.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(L.getLatitude(), L.getLongitude()), 16));
                    EventBus.getDefault().post(new LatLonModel(L.getLatitude(), L.getLongitude()));

                    startService(new Intent(BaseApplication.baseApp, SmartLocationFetchAddressService.class));

                });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        SmartLocation.with(this).location().stop();
    }
}
