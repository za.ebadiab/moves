package advance.android.moves.services;

public class LatLonModel {

    double lat , lon;

    public LatLonModel(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }
}
