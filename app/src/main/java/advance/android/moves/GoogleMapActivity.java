package advance.android.moves;

import android.Manifest;
import android.content.Intent;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import advance.android.moves.customViews.MyTextView;
import advance.android.moves.services.AddressModel;
import advance.android.moves.services.LatLonModel;
import advance.android.moves.services.SmartLocationFetchAddressService;
import advance.android.moves.services.SmartLocationService;
import advance.android.moves.utils.BaseApplication;
//import advance.android.moves.utils.PublicMethods;
import advance.android.moves.utils.PublicMethods;
import saman.zamani.persiandate.PersianDate;

public class GoogleMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    MyTextView location;
    ArrayList<Marker> markersArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        location = findViewById(R.id.location);

    }

    @Override
    protected void onResume() {
        super.onResume();
        setStyle();
        EventBus.getDefault().register(this);


    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe
    public void onReceivedAddress(AddressModel addressModel) {

        location.setText(addressModel.addressClass.getAddressLine(0));
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true);

        setStyle();

        getPermission();

        getLocationByMapMovement();
    }

    void getLocationByMapMovement() {
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                double lat = mMap.getCameraPosition().target.latitude;
                double lon = mMap.getCameraPosition().target.longitude;

                EventBus.getDefault().post(new LatLonModel(lat, lon));

                 startService(new Intent(BaseApplication.baseApp, SmartLocationFetchAddressService.class));

                removeMarkers();
                getRandomMarkers(lat, lon);

            }
        });

    }

    void getPermission() {


        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION

                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                BaseApplication.mMap = mMap;

                 startService(new Intent(BaseApplication.baseApp, SmartLocationService.class));

                 PublicMethods.Companion.toast(getApplication(), getString(R.string.GPS_permitted));
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {


            }

        }).check();

    }


    /*
       void cedarMapGetAddress(com.mapbox.mapboxsdk.geometry.LatLng coordinate) {

           if (CedarMaps.getInstance() == null)
               Log.d("my log", "CedarMaps.getInstance()==null");

           CedarMaps.getInstance().reverseGeocode(
                   coordinate,
                   new ReverseGeocodeResultListener() {
                       @Override
                       public void onSuccess(@NonNull ReverseGeocode result) {
                           Log.d("my log", "address= " + result.getAddress() + " " + result.getPlace());
                           location.setText(result.getAddress() + " " + result.getPlace());

                       }

                       @Override
                       public void onFailure(@NonNull String errorMessage) {
                           Log.d("my log", "address faild= " + coordinate.getLatitude() + " , " + coordinate.getLongitude());

                       }
                   });
       }
   */
    void setStyle() {
        try {
            PersianDate date = new PersianDate();

            date.getTime();

            if (date.getHour() <= 6 || date.getHour() >= 18)
                mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_mosy_night));

            else {
                mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_accl_day));

            }

        } catch (Exception e) {

        }
    }


    void getRandomMarkers(double lat, double lon) {

        markersArrayList = new ArrayList<>();

        MarkerOptions markerOptions;

        Marker mMarker;
        LatLng loc = null;
        Random randRotation, randCount;
        randRotation = new Random();
        randCount = new Random();

        for (int i = 0; i < randCount.nextInt(2) + 1; i++) {
            loc = new LatLng(lat + Math.random() / 1000, lon + Math.random() / 1000);

            markerOptions = new MarkerOptions().position(loc)
                    .title("Current Location")
                    .snippet("Thinking of finding some thing...")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.snapp_map))
                    .rotation(randRotation.nextInt(360) + 1);

            mMarker = mMap.addMarker(markerOptions);
            markersArrayList.add(mMarker);

        }
        for (int i = 0; i < randCount.nextInt(2) + 1; i++) {

            loc = new LatLng(lat + Math.random() / 1000, lon - Math.random() / 1000);

            markerOptions = new MarkerOptions().position(loc)
                    .title("Current Location")
                    .snippet("Thinking of finding some thing...")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.snapp_map))
                    .rotation(randRotation.nextInt(360) + 1);

            mMarker = mMap.addMarker(markerOptions);
            markersArrayList.add(mMarker);
        }
        for (int i = 0; i < randCount.nextInt(2) + 1; i++) {

            loc = new LatLng(lat - Math.random() / 1000, lon - Math.random() / 1000);

            markerOptions = new MarkerOptions().position(loc)
                    .title("Current Location")
                    .snippet("Thinking of finding some thing...")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.snapp_map))
                    .rotation(randRotation.nextInt(360) + 1);

            mMarker = mMap.addMarker(markerOptions);
            markersArrayList.add(mMarker);


        }

    }

    /*
        void carRotationToDest(double latTarget, double lonTarget, double latFrom, double lonFrom){

                Location prevLoc = new Location("service Provider");
                prevLoc.setLatitude(latFrom);
                prevLoc.setLongitude(lonFrom);
                Location newLoc = new Location("service Provider");
                newLoc.setLatitude(latTarget);
                newLoc.setLongitude(lonTarget);
                float bearing = prevLoc.bearingTo(newLoc);
             //   mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latTarget, lonTarget))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.snapp_map))
                        .anchor(0.5f, 0.5f)
                        .rotation(bearing)
                        .flat(true));


              //  oldlong = newlong;


        }
    */
    void removeMarkers() {
        if (markersArrayList != null && markersArrayList.size() != 0) {
           for (int i = 0; i < markersArrayList.size(); i++)
                markersArrayList.get(i).remove();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopService(new Intent(this, SmartLocationService.class));
        stopService(new Intent(this, SmartLocationFetchAddressService.class));

    }
}
